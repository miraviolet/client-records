const app = new Vue({
  el: '#app',
  data: {
    clients: {}
  },
  mounted: function() {
    fetch('https://us-central1-client-records.cloudfunctions.net/get') // The link to the Database 
  .then(res => res.json())
    .then(body => this.clients = body)
  }
})